/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mini_paint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jonny <josaykos@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/15 15:58:40 by jonny             #+#    #+#             */
/*   Updated: 2020/09/17 16:22:18 by jonny            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>

typedef struct draw {
	int width;
	int height;
	char *matrix;
} draw;

typedef struct circle {
	char type;
	float x;
	float y;
	float radius;
	char background;
} circle;

void ft_putstr(char *str)
{
	while (*str)
	{
		write(1, &(*str), 1);
		str++;
	}
}

void print_draw(draw *zone)
{
	int i;

	i = 0;
	while (i < zone->height)
	{
		write(1, zone->matrix + (i * zone->width), zone->width);
		write(1, "\n", 1);
		i++;
	}
}

int check_circle(float x, float y, circle *circle)
{
	float   distance;
	float   distance_sqrt;

	distance_sqrt = sqrtf(powf(x - circle->x, 2) + powf(y - circle->y, 2));
	distance = distance_sqrt - circle->radius;
	if (distance <= 0.00000000)
	{
		if (distance <= -1.00000000)
			return (1); // Inside
		return (2); // Border
	}
	return (0);
}

void execute_one(circle *circle, draw *zone, int x, int y)
{
	int inside_circle;

	inside_circle = check_circle((float)x, (float)y, circle);
	if (inside_circle == 2 || (inside_circle == 1 && circle->type == 'C'))
		zone->matrix[x + y * zone->width] = circle->background;
}

int apply_op(circle *circle, draw *zone)
{
	int i;
	int j;

	if (circle->radius <= 0.00000000 || (circle->type != 'C' && circle->type != 'c'))
		return (1);
	i = 0;
	while (i < zone->width)
	{
		j = 0;
		while (j < zone->height)
			execute_one(circle, zone, i, j++);
		i++;
	}
	return (0);
}


int draw_zone(FILE *file, draw *zone)

{
	int i;
	char background;

	if (fscanf(file,"%d %d %c\n", &zone->width, &zone->height, &background) == 3)
	{
		if (((zone->width < 1 || zone->width > 300) || zone->height < 1) ||
															zone->height > 300)
			return (1);
		zone->matrix = (char *)malloc(zone->width * zone->height);
		if (!zone->matrix)
			return (1);
		i = 0;
		while (i < zone->width * zone->height)
			zone->matrix[i++] = background;
		return (0);
	}
	return (1);
}

int execute(FILE *file)
{
	int ret;
	circle circle;
	draw zone;

	if (!draw_zone(file, &zone))
	{
		ret = fscanf(file,"%c %f %f %f %c\n", &circle.type, &circle.x,
								&circle.y, &circle.radius, &circle.background);
		while (ret == 5)
		{
			if (apply_op(&circle, &zone))
				return (1);
			ret = fscanf(file,"%c %f %f %f %c\n", &circle.type, &circle.x,
								&circle.y, &circle.radius, &circle.background);
		}
		if (ret == -1)
		{
			print_draw(&zone);
			free(zone.matrix);
			return (0);
		}
	}
	return (1);
}

int main(int argc,char **argv)
{
	FILE *file;

	if (argc == 2)
	{
		file = fopen(argv[1], "r");
		if (file && !execute(file))
		{
			fclose(file);
			return (0);
		}
		ft_putstr("Error: Operation file corrupted\n");
	}
	else
		ft_putstr("Error: argument\n");
	return (1);
}
