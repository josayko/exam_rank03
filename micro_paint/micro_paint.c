/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   micro_paint.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jonny <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 11:44:19 by jonny             #+#    #+#             */
/*   Updated: 2021/01/12 11:44:23 by jonny            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define XTOP_LEFT (rect->x)
#define YTOP_LEFT (rect->y)
#define XTOP_RIGHT (rect->x + rect->width)
#define YBOTTOM_LEFT (rect->y + rect->height)

typedef struct draw
{
	int width;
	int height;
	char *matrix;
} draw;

typedef struct rectangle
{
	char type;
	float x;
	float y;
	float width;
	float height;
	char background;
} rectangle;

void ft_putstr(char *str)
{
	while (*str)
	{
		write(1, &(*str), 1);
		str++;
	}
}

void print_draw(draw *zone)
{
	int i;

	i = 0;
	while (i < zone->height)
	{
		write(1, zone->matrix + (i * zone->width), zone->width);
		write(1, "\n", 1);
		i++;
	}
}

int check_rectangle(float x, float y, rectangle *rect)
{
	if (((x < XTOP_LEFT || x > XTOP_RIGHT) || y < YTOP_LEFT) ||
															y > YBOTTOM_LEFT)
		return (0);
	if ((x - XTOP_LEFT < 1.00000000 || XTOP_RIGHT - x < 1.00000000) ||
				(y - YTOP_LEFT < 1.00000000 || YBOTTOM_LEFT - y < 1.00000000))
		return (2);// Border
	return (1); // Inside
}

void execute_one(rectangle *rect, draw *zone, int x, int y)
{
	int inside_rect;

	inside_rect = check_rectangle((float)x, (float)y, rect);
	if (inside_rect == 2 || (inside_rect == 1 && rect->type == 'R'))
		zone->matrix[x + y * zone->width] = rect->background;
}

int apply_op(rectangle *rect, draw *zone)
{
	int i;
	int j;

	if ((rect->width <= 0.00000000 || rect->height <= 0.00000000) ||
									(rect->type != 'R' && rect->type != 'r'))
		return (1);
	i = 0;
	while (i < zone->width)
	{
		j = 0;
		while (j < zone->height)
			execute_one(rect, zone, i, j++);
		i++;
	}
	return (0);
}

int draw_zone(FILE *file, draw *zone)
{
	int i;
	char background;

	if (fscanf(file, "%d %d %c\n", &zone->width, &zone->height, &background) == 3)
	{
		if (((zone->width < 1 || zone->width > 300) || zone->height < 1) ||
															zone->height > 300)
			return (1);
		zone->matrix = (char*)malloc(zone->width * zone->height);
		if (!zone->matrix)
			return (1);
		i = 0;
		while (i < zone->width * zone->height)
			zone->matrix[i++] = background;
		return (0);
	}
	return (1);
}

int execute(FILE *file)
{
	int ret;
	rectangle rect;
	draw zone;

	if (!draw_zone(file, &zone))
	{
		ret = fscanf(file, "%c %f %f %f %f %c\n", &rect.type, &rect.x, &rect.y,
				&rect.width, &rect.height, &rect.background);
		while (ret == 6)
		{
			if (apply_op(&rect, &zone))
				return (1);
			ret = fscanf(file, "%c %f %f %f %f %c\n", &rect.type, &rect.x, &rect.y,
					&rect.width, &rect.height, &rect.background);
		}
		if (ret == -1)
		{
			print_draw(&zone);
			free(zone.matrix);
			return (0);
		}
	}
	return (1);
}

int main(int argc, char **argv)
{
	FILE *file;

	if (argc == 2)
	{
		file = fopen(argv[1], "r");
		if (file && !execute(file))
		{
			fclose(file);
			return (0);
		}
		ft_putstr("Error: Operation file corrupted\n");
	}
	else
		ft_putstr("Error: argument\n");
	return (1);
}
